#include<stdio.h>
#include<conio.h>
#include<math.h>
#include<stdlib.h>
#include<graphics.h>

#define BOOL int
#define true 1
#define false 0
#define VRETRACE 0x08
#define INPUT_STATUS 0x03da

/*
*    3D Mesh example
*
*    uses Borland's Graphics Interface for rendering
*    File \turboc\bgi\bgidemo.c has lots of information.
*    Page flipping operational using VGA MODE 10h
*    640 by 350 , 16 colour graphics mode.
*                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
*    Uses Matrix Transformations transformations
*    i.e.  ( local , translation, scaling )
*
*
*  Coded by the Grid as a learning experience in the wonders of C.
*  www.thegrid.site90.net.
*
*
*/

/* GLOBAL Declarations */

/*
*
*  Animation and screen swapping variables, procedure prototype.
*
*/
BOOL screenpage;
int seq;
void flip_page();

/*
*
*  Structures, definitions and floating point fix proc.
*
*/


typedef struct vertex // homogeneous coordinate definition
{
        int x, y, w;

} VERTEX;

typedef struct vertex3d // 3D coordinate definition
{
        double x, y, z;

} VERTEX3D;

typedef struct face // triangular faces - indexes to a vertex buffer for the object.
{
        int a, b, c;

} FACE;

typedef struct object
{
        int type, numvert, numface;
        FACE *pface;
        VERTEX3D *pmesh;
        VERTEX loc, rot;
        VERTEX3D *pscene;

} OBJECT;

/* 
*  Floating point math fixes.
*  Never needs to be called.
*/

static void forcefloat(float *p) {
        float f = *p;
        forcefloat(&f);
}

typedef double MATRIX3x3[3][3];

typedef double MATRIX4x4[4][4];

/* 2D Function */
// Probably dont require this now need to check!!!

VERTEX Trans2D( MATRIX3x3 *m, VERTEX *a)  //changed to pointers from 2D version
{

  VERTEX b;

  b.x =   (*m)[0][0] * a->x +  (*m)[0][1] *  a->y + (*m)[0][2] *  a->w + 320;
  b.y =   (*m)[1][0] * a->x +  (*m)[1][1] *  a->y + (*m)[1][2] *  a->w +175 ;
  b.w= 1;
  return b;
}

/*3D Functions */

/*
*
*  Trans3D :Transformation of vertex3d using matrices
*
*  Cross Product and Dot Product Vector calc's.
*  for finding visible surfaces, ( bugged!! )
*
*/


VERTEX3D Trans3D( MATRIX4x4 *m, VERTEX3D *a)  
{

  VERTEX3D b;

  b.x =   (*m)[0][0] * (a->x) +  (*m)[0][1] *   (a->y) + (*m)[0][2] *   (a->z) + (*m)[0][3] ;
  b.y =   (*m)[1][0] * (a->x) +  (*m)[1][1] *   (a->y) + (*m)[1][2] *   (a->z) + (*m)[1][3] ;
  b.z =   (*m)[2][0] * (a->x) +  (*m)[2][1] *   (a->y) + (*m)[2][2] *   (a->z) + (*m)[2][3] ;

// w = 1 but is not stored.
  return b;

}

VERTEX3D Cross_Product( VERTEX3D *a, VERTEX3D *b, VERTEX3D *c)


{

VERTEX3D v1,v2,cp;

v1.x = (*b).x - (*a).x;
v1.y = (*b).y - (*a).y;
v1.z = (*b).z - (*a).z;

v2.x = (*c).x - (*a).x;
v2.y = (*c).y - (*a).y;
v2.z = (*c).z - (*a).z;

cp.x = v1.y * v2.z - v1.z * v2.y;
cp.y = v1.z * v2.x - v1.x * v2.z;
cp.z = v1.x * v2.y - v1.y * v2.x;


return cp;

        }


float Dot_Product( VERTEX3D *a, VERTEX3D *b )

{

   return (a->x * b->x) + ( a->y * b->y) + (a->z * b->z);

}


BOOL FaceVisible( VERTEX3D *a, VERTEX3D *b, VERTEX3D *c )
{


        VERTEX3D cp, view;
        float dot;

        cp=Cross_Product( a ,b ,c );
        view.x=(*a).x;
        view.y=(*a).y;
        view.z=(*a).z;
        dot=Dot_Product(&view, &cp);
        return ( dot <= 0 );

}


/*  Procedures Declarations */

/* 2D matrices prototypes*/

void Triangle (VERTEX, VERTEX, VERTEX );  // This is what does our drawing!

/*3D matrices prototypes*/

void Mult3DMatrix( MATRIX4x4 *product, MATRIX4x4 *multer );

void ObjectMatrix ( MATRIX4x4 *m, int degX, int degY, int degZ, int transX, int transY, int transZ, int scale);

void DrawPerspectiveTriangle( VERTEX3D*, VERTEX3D*, VERTEX3D* );


/* Main Programme start */
int main() {


//2 D variable assignments
  MATRIX3x3 m1;

//3 D variable assignments

  MATRIX4x4 m3d;

  int gd = VGA, gm=VGAMED;
  int x = 0, radius;
  double kscale;

  VERTEX3D a,b,c, temp;
  VERTEX3D *temp_ptr;

// 3d definitions working on.


        VERTEX3D mesh[] = { {-1.0,1.0,-1.0},
        { 1.0, 1.0,-1.0},
        { 1.0,-1.0,-1.0},
        {-1.0,-1.0,-1.0},
        {-1.0, 1.0, 1.0},
        { 1.0, 1.0, 1.0},
        { 1.0,-1.0, 1.0},
        {-1.0,-1.0, 1.0} }; //array of cube2d vertices

       FACE faces[] = { { 2, 1, 0 }, // ccw
       { 2, 0, 3 }, //ccw
       { 7, 6, 5 }, //cw
       { 7, 5 , 4  },//cw
       { 0, 7 , 4   }, //cw
       { 7, 0, 3 }, //cw
       { 2 , 1, 5 },  //cw
       { 6, 2, 5 },  //cw
       { 5, 1,0 },  //cw
       { 4,5,0 },    //cw
       { 3,2,7 },   //cw
       { 2, 6, 7 }          };  // array of cube2d faces as traingles


        OBJECT cube3d;

        cube3d.type=0;
        cube3d.numvert=8;
        cube3d.numface=12;
        cube3d.pface=faces;
        cube3d.pmesh=mesh;
        cube3d.loc.x = 0;
        cube3d.loc.y = 0;
        cube3d.loc.w = 0;
        cube3d.rot.x = 0;
        cube3d.rot.y = 0;
        cube3d.rot.w = 0;
        cube3d.pscene= NULL;




        initgraph(&gd, &gm, "c:\\turboc\\bgi");

        flip_page();

        setcolor(WHITE);
        settextstyle(DEFAULT_FONT,HORIZ_DIR,2);
        outtextxy(10,320,"3D test");
        flip_page();
        delay(3000); // getch();

        seq=0;
        flip_page();

   while (!kbhit() )
   {


   ObjectMatrix ( &m3d, seq, seq, 30, 0, 0, -40-(35*sin(6.28*seq/360)) , 1);

   for ( x=0 ; x < 12; x++)
      {
        setcolor(x/2);


         temp=(mesh[faces[x].a ]);
         temp_ptr=&temp;
         a=Trans3D(&m3d ,temp_ptr );
         temp=(mesh[faces[x].b ]);
         b= Trans3D(&m3d ,temp_ptr );
         temp=(mesh[faces[x].c ]);
         c= Trans3D(&m3d ,temp_ptr  );


        if ( FaceVisible( &a, &b, &c ))
        {
         DrawPerspectiveTriangle( &a , &b , &c);
        }

      }
   flip_page();
   ( seq > 359 ) ? seq=0 : ++seq;

  }

        delay(3000);

        setcolor(WHITE);
        settextstyle(DEFAULT_FONT,HORIZ_DIR,2);
        outtextxy(10,320,"3D Test completed");
        flip_page();

        delay(3000); // getch();

        closegraph();

        printf ("End of Program.\n\n");
        return 0;
}

/*
*
*  Define a triangles points for drawing onto the screen.
*
*/

void Triangle (VERTEX a, VERTEX b, VERTEX c)
{
    int points[8];
    points[0]= a.x;    points[1]= a.y;
    points[2]= b.x;    points[3]= b.y;
    points[4]= c.x;    points[5]= c.y;
    points[6]= a.x;    points[7]= a.y;
    drawpoly(4,points);
}


/*
*
*  Swap in active page to view and clear visible pages for next screen update.
*
*/

void flip_page()
{
  int address;

while ( (inp(INPUT_STATUS) & VRETRACE) ) {};

  if (screenpage) {
        setactivepage(1);
        setvisualpage(0);
        screenpage=false;
        address = 28000;
         // we want to clear page 1 here at offset 28000 bytes
        }
  else
       {
        setactivepage(0);
        setvisualpage(1);
        screenpage=true;
        address = 0;
        // we want to clear page 0 here at offset 0 bytes
       }

while ( !(inp(INPUT_STATUS) & VRETRACE) ) {};


           _asm {
                 mov ax,0a000h
                 mov es,ax
                 mov dx,03c4h
                 mov ax,0f02h
                 out dx,ax
                 mov di,address
                 mov cx,18000
                 mov ax,0
                 rep stosw
                }


}

/*
*
* 3DMultMatrix
*
* Multipy 4x4 matrices
*
*   product = product * multer
* 
*
*/

void Mult3DMatrix( MATRIX4x4 *product, MATRIX4x4 *multer)
/* 4 by 4 matrix multiplication - product = product * multer */
{

MATRIX4x4 temp;

int x,y,z;
double sum;

  for (x=0 ; x<4 ; ++x)
      for (y=0; y< 4 ; ++y )
    {
      sum=0;
      for (z=0; z<4; ++z)
        sum += (*product)[z][y] * (*multer)[x][z] ;
     temp[x][y]=sum;
    }

  for (x=0 ; x<4 ; ++x)
      for (y=0; y< 4 ; ++y )
        {
        (*product)[x][y]=temp[x][y];
        }

}

/*
*
* ObjectMatrix
*
* Construct the matrix for each individual object to move it from Object Space
* to world space.
*
* method:- Build local Scale matrix then carry out matrix multiplication as 
* (TxTyTz) (RxRyRx) (S)
* all scaling is the same on each axis.
*
*/

void ObjectMatrix ( MATRIX4x4 *m, int degX, int degY, int degZ, int transX, int transY, int transZ, int scale)

{

 MATRIX4x4 local;
 double rad; //radians
 double c;  //cosines
 double s; //sines

//Build the scaling matrix directly into m

 (*m)[0][0]=scale; (*m)[0][1]=0; (*m)[0][2]=0; (*m)[0][3]=0;
 (*m)[1][0]=0; (*m)[1][1]=scale; (*m)[1][2]=0; (*m)[1][3]=0;
 (*m)[2][0]=0; (*m)[2][1]=0; (*m)[2][2]=scale; (*m)[2][3]=0;
 (*m)[3][0]=0; (*m)[3][1]=0; (*m)[3][2]=0; (*m)[3][3]=1;


// Rotations

  if (degX != 0 ) {

          rad = 6.283185308 / (360.0/ degX);
          c = cos(rad);
          s = sin(rad);

         local[0][0]=1; local[0][1]=0; local[0][2]=0; local[0][3]=0;
         local[1][0]=0; local[1][1]=c; local[1][2]=s; local[1][3]=0;
         local[2][0]=0; local[2][1]=-s; local[2][2]=c; local[2][3]=0;
         local[3][0]=0; local[3][1]=0; local[3][2]=0; local[3][3]=1;

         Mult3DMatrix( m, &local);

  }

  if (degY != 0 ) {

          rad = 6.283185308 / (360.0/ degY);
          c = cos(rad);
          s = sin(rad);
                                                   
         local[0][0]=c; local[0][1]=0; local[0][2]=-s; local[0][3]=0;
         local[1][0]=0; local[1][1]=1; local[1][2]=0; local[1][3]=0;
         local[2][0]=s; local[2][1]=0; local[2][2]=c; local[2][3]=0;
         local[3][0]=0; local[3][1]=0; local[3][2]=0; local[3][3]=1;

         Mult3DMatrix( m, &local);

  }

  if (degZ != 0 ) {

          rad = 6.283185308 / (360.0/ degZ);
          c = cos(rad);
          s = sin(rad);

         local[0][0]=c; local[0][1]=s; local[0][2]=0; local[0][3]=0;
         local[1][0]=-s; local[1][1]=c; local[1][2]=0; local[1][3]=0;
         local[2][0]=0; local[2][1]=0; local[2][2]=1; local[2][3]=0;
         local[3][0]=0; local[3][1]=0; local[3][2]=0; local[3][3]=1;

         Mult3DMatrix( m, &local);

  }

// Translations

 local[0][0]=1; local[0][1]=0; local[0][2]=0; local[0][3]=transX;
 local[1][0]=0; local[1][1]=1; local[1][2]=0; local[1][3]=transY;
 local[2][0]=0; local[2][1]=0; local[2][2]=1; local[2][3]=transZ;
 local[3][0]=0; local[3][1]=0; local[3][2]=0; local[3][3]=1;

 Mult3DMatrix( m, &local);

 }


VERTEX Screen( VERTEX3D v )
{

  VERTEX p;
  double recipZ = 1 / v.z;

   p.x = 320 - ( 500 *  v.x * recipZ   );
   p.y = (175 + (500 *   v.y * recipZ   ));
   p.w = 1; // v.z;
  return p;

}


void DrawPerspectiveTriangle( VERTEX3D *a1 , VERTEX3D *b2, VERTEX3D *c3 )
{


 VERTEX t,y,e;
  t = Screen( *a1 );
  y = Screen( *b2 );
  e = Screen( *c3 );

  Triangle( t, y , e);

}


